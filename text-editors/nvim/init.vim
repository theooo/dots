"###########################################################
" TABLINE
"###########################################################

set showtabline=0
" set tabline=%r\ %m\ %t\ %y\ L:\%L\ W: set tabline+=%{wordcount()[\"words\"]}\ PC:\%p%%
" highlight tablinefill cterm=none ctermbg=none ctermfg=lightcyan

"###########################################################
" STATUSLINE
"###########################################################

set laststatus=2 " set laststatus=2
" set statusline=%{strftime('%H:%M\ ')}
set statusline=%r\ %m\ %t\ %y\ L:\%l/%L\ W:
set statusline+=%{wordcount()[\"words\"]}\ PC:\%p%%\ 
set statusline+=%5.(%l,%c%V%)
highlight statusline cterm=none ctermbg=none ctermfg=cyan
set rulerformat+=%55(%{strftime('%a\ %b\ %e\ %I:%M\ %p')}\ %5l,%-6(%c%V%)\ %P%)

"###########################################################
" SET
"###########################################################

" set list
set nu rnu
set nocompatible
set termguicolors
set nohlsearch incsearch
set foldmethod=manual
set cursorcolumn " cursorline
set background=dark
set history=1000
set autowrite
set lazyredraw
set tabstop=2 softtabstop=2 shiftwidth=2
set expandtab
set ignorecase smartcase
set smartindent
set scrolloff=9
filetype plugin on
set omnifunc=syntaxcomplete#Complete
syntax on
set listchars=trail:~,extends:>,precedes:<,space:¯

"###########################################################
" MAP
"###########################################################

inoremap jk <ESC>
nnoremap j gj
nnoremap k gk
nnoremap <C-a> VG
vnoremap <C-y> "+y
nnoremap <C-p> "+P
nnoremap <F3> :!pdfdown % <enter><enter>
autocmd FileType html,markdown inoremap </ </<C-x><C-o>
autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent loadview 

inoremap " ""<left>
"inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

"###########################################################
" VIM-PLUG
"###########################################################

call plug#begin()
Plug 'mattn/emmet-vim'
Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'justinmk/vim-sneak'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
call plug#end()

"###########################################################
" PLUGIN-RELATED STUFF
"###########################################################

let g:sneak#label = 1
let g:sneak#use_ic_scs = 1

lua << EOF
require'colorizer'.setup()
EOF
