; ## HIDE ##
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

; ## FONT ##
(add-to-list 'default-frame-alist '(font . "VictorMono Nerd Font Mono-15"))

; ## CUA ##
(cua-mode t)
(setq cua-auto-tabify-rectangles nil)
(transient-mark-mode 1)
(setq cua-keep-region-after-copy t)

; ## LINE NUMBERS ##
; (global-display-line-numbers-mode)
; (setq display-line-numbers-type 'relative)

; ## STARTUP SCREEN
(setq inhibit-startup-message t)

; ## BELL ##
(setq ring-bell-function 'ignore)

; ## UTF-8 ##
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)

; ## Y/N
(defalias 'yes-or-no-p 'y-or-n-p)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

; ## VTERM ##
(require 'use-package)
(use-package vterm
      :ensure t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(evil vterm use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(set-frame-parameter (selected-frame) 'alpha '(85 . 50))
 (add-to-list 'default-frame-alist '(alpha . (85 . 50)))

; ## EVIL

(require 'evil)
(evil-mode 1)
