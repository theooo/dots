#!/bin/sh

date() {
	date=$(date "+%Y %A %B %d %H:%M:%S %j")
	echo $date
}

battery() {
	percentage=$(cat /sys/class/power_supply/BAT0/capacity)
       	status=$(head -c1 /sys/class/power_supply/BAT0/status)	
	echo PC $percentage $status
}

bright() {
	bright=$(echo $[$(cat /sys/class/backlight/amdgpu_bl0/brightness)*100/255])
	echo BR $bright
}

volume() {
	volume=$(pactl list sinks | grep -E -o '.{,2}[0-9]%' | head -n1)
	echo VOL $volume
}

wlan() {
	hotspot=$(connmanctl services | head -n1 | awk '{print $2}')
	rx=$(cat /sys/class/net/wlp1s0/statistics/rx_bytes)
	tx=$(cat /sys/class/net/wlp1s0/statistics/tx_bytes)
	datacap=$(echo $[$rx/1024/1024+$tx/1024/1024])
	echo $hotspot $datacap\M 
}

memory() {
	ram=$(free -h | grep "Mem" | awk '{print $3}' )
	echo MEM $ram
}

herbstluft() {
	layout=$(herbstclient layout | awk '{print $2}')
	tag=$(herbstclient tag_status | grep -o '#.')
	echo $layout $tag
}

while true; do
	echo -e "%{l} $(date) $(herbstluft) $(memory) $(wlan) %{r} $(bright) $(volume) $(battery)"
	sleep 1
done
