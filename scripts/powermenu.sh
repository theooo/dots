#!/bin/bash

prompt=$(printf "poweroff"'\n'"reboot"'\n'"logout" | fzf)
  case $prompt in
    "reboot") reboot ;;
    "poweroff") poweroff ;;
    "logout") bspc quit ;;
esac
