#!/usr/bin/python3

import argparse
from urllib import request
from bs4 import BeautifulSoup

parser = argparse.ArgumentParser(description="translayte.py")
parser.add_argument('-f', '--from', dest='frm', metavar='', type=str, help="language to be translated from")
parser.add_argument('-t', '--to', metavar='', type=str, help="language to be translated to")
parser.add_argument('-w', '--word', metavar='', type=str, help="word/phrase to be converted")
args = parser.parse_args()

if args.frm: frm = args.frm
else: frm = input("Language to be translated from: ")
if args.to: to = args.to
else: to = input("Language to be translated to: ")
if args.word: word = args.word
else: word = input("Word/phrase to be translated: ")
word = word.replace(' ','+')

html = request.urlopen(f'https://simplytranslate.org/?engine=google&sl={frm}&tl={to}&text={word}')
soup = BeautifulSoup(html.read(), "html.parser")
transl = soup.find("textarea", class_="translation").text
print("Translation: " + transl)
