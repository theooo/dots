#!/bin/bash

echo -e "\n\e[4mDICTIONARY FOR NOOBS\e[0m\n"

if [[ -z $(command -v jq) ]]; then echo "jq not installed" && exit; fi

if [[ -z $1 ]]; then echo -e "\e[4mNote:\e[0m Provide word as argument\n" && exit; fi

word=$1

curl -s https://api.dictionaryapi.dev/api/v2/entries/en_US/"$word" >> /tmp/"$word".json

echo -ne "\e[4mWord:\e[0m"\ $(jq '.[].word' /tmp/"$word".json) "\n"\
"\e[4mPhonetics:\e[0m"\ $(jq '.[].phonetics[0].text' /tmp/"$word".json) "\n"\
"\e[4mDefinition:\e[0m"\ $(jq '.[].meanings[0].definitions[0].definition' /tmp/"$word".json | grep -v null) "\n"\
"\e[4mSynonyms:\e[0m"\ $(jq '.[].meanings[].definitions[].synonyms' /tmp/"$word".json | grep -v null | tr -d '[]') "\n"

rm /tmp/"$word".json
