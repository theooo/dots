# cat ~/.config/qutebrowser/config.py

# Change the argument to True to still load settings configured via autoconfig.yml
config.load_autoconfig(False)

# Which cookies to accept. | Type: String
# Valid values: all, no-3rdparty, no-unknown-3rdparty, never
config.set('content.cookies.accept', 'all', 'chrome-devtools://*')
config.set('content.cookies.accept', 'all', 'devtools://*')

config.set('content.private_browsing', False)

# Value to send in the `Accept-Language` header. Note that the value
# read from JavaScript is always the global value. | Type: String
config.set('content.headers.accept_language', '', 'https://matchmaker.krunker.io/*')

# User agent to send. | Type: FormatString
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version}', 'https://web.whatsapp.com/')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/{webkit_version} (KHTML, like Gecko) {upstream_browser_key}/{upstream_browser_version} Safari/{webkit_version} Edg/{upstream_browser_version}', 'https://accounts.google.com/*')
config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36', 'https://*.slack.com/*')

# Load images automatically in web pages. | Type: Bool
config.set('content.images', True, 'chrome-devtools://*')
config.set('content.images', True, 'devtools://*')

# Enable JavaScript. | Type: Bool
c.content.javascript.enabled = False
config.set('content.javascript.enabled', True, 'chrome-devtools://*')
config.set('content.javascript.enabled', True, 'devtools://*')
config.set('content.javascript.enabled', True, 'chrome://*/*')
config.set('content.javascript.enabled', True, 'qute://*/*')
config.set('content.javascript.enabled', False, '*://help/*')

config.set('content.javascript.enabled', True, '*://gitlab.com/*')
config.set('content.javascript.enabled', True, '*://duckduckgo.com/*')
config.set('content.javascript.enabled', True, '*://piped.kavin.rocks/*')

# Allow websites to show notifications. | Type: BoolAsk
# Valid values: true, false, ask
config.set('content.notifications.enabled', False, 'https://meet.google.com')
config.set('content.notifications.enabled', False, 'https://www.reddit.com')

# Editor (and arguments) to use for the `edit-*` commands. | Type: ShellCommand
# * `{file}`: Filename of the file to be edited. 
# * `{line}`: Line in which the caret is found in the text.
# * `{column}`: Column in which the caret is found in the text.
# * `{line0}`: Same as `{line}`, but starting from index 0.
# * `{column0}`: Same as `{column}`, but starting from index 0.
c.editor.command=['kitty','-e',"'vim' '{file}'"]

c.url.start_pages = 'about:blank'
c.url.default_page = 'about:blank'
c.window.hide_decoration = False
c.zoom.default = '180%'
c.scrolling.bar = 'overlay'
c.content.autoplay = False
c.statusbar.show = "in-mode"
c.downloads.remove_finished = 2000
c.scrolling.smooth = True
c.tabs.background = True
c.tabs.favicons.show = 'never'
c.tabs.title.alignment = 'center'
c.tabs.position = 'bottom' # top, right, left
c.tabs.show = 'multiple' # always, never, switching
c.fonts.default_family = 'VictorMono Nerd Font Mono'
c.fonts.default_size = '13pt'
c.fonts.web.family.fixed = 'JetBrainsMono Nerd Font'
c.fonts.web.family.standard = 'SF Pro Text'
c.fonts.web.family.sans_serif = 'SF Pro Text'
c.fonts.hints = 'bold 17pt default_family'
c.hints.border = '2px solid white'
c.colors.tabs.bar.bg = '#000000'
c.colors.webpage.bg = 'black'
c.colors.webpage.preferred_color_scheme = 'dark' # auto, light
c.colors.hints.fg = 'white'
c.colors.hints.bg = 'black'
c.colors.hints.match.fg = 'yellow'
c.colors.completion.scrollbar.fg = "#ffffff"
c.completion.height = "45%"
c.colors.webpage.darkmode.enabled = True
c.content.geolocation = False
c.statusbar.position = 'top'
c.tabs.position = 'top'

c.url.searchengines = {
        'DEFAULT': 'https://duckduckgo.com/?q={}',
        'ol': 'https://olam.in/Dictionary/?action=en_ml&q={}',
        'metager': 'https://metager.org/meta/meta.ger3?eingabe={}',
        'mer': 'https://merriam-webster.com/dictionary/{}',
        'AUR': 'https://aur.archlinux.org/packages/?O=0&K={}',
        'archwiki': 'https://wiki.archlinux.org/index.php?search={}',
        'urbandict': 'https://www.urbandictionary.com/define.php?term={}'
        }

config.bind('J', 'tab-prev')
config.bind('K', 'tab-next')
# config.bind('yp', 'hint links yank {hint-url}')
config.bind('M', 'hint links spawn mpv --ytdl-format=bestvideo[height=?360]+bestaudio {hint-url}')
config.bind('w3', 'hint links spawn sexy -e w3m {url}')
config.bind('za', 'config-cycle tabs.show always never')
config.bind('zz', 'config-cycle statusbar.show always never;; config-cycle tabs.show always never')
config.bind('aa', 'bookmark-add')
config.bind('ap', 'print --pdf {title}.pdf')
config.bind('ah', 'history-clear')
config.bind('al', 'open -t qute://bookmarks')
config.bind('ad', 'set-cmd-text :download --mhtml')
config.bind('ai', 'config-cycle content.images true false;; reload')
config.bind('aj', 'config-cycle -p -t -u *://{url:host}/* content.javascript.enabled ;; reload')
