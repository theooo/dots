##### history and all that stuff ###################################

export HISTFILE=~/.config/history/zhist
export HISTSIZE=100000000000000
export SAVEHIST=100000000000000
setopt INC_APPEND_HISTORY

##### Basic auto/tab complete ######################################

autoload -U compinit
autoload -Uz colors && colors
bindkey -v
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)	# Include hidden files.

##### enable comments in interactive shell #########################

setopt interactivecomments

### aliases ########################################################

alias time="/usr/bin/time -f '\nTime: %E'"
alias wget="wget --hsts-file=/dev/null"
alias sxiv="sxiv -b -p"
alias rm="trash"
alias rsync="rsync -r --info=progress2 -h"
alias grep="grep -i --color=auto"
alias l="ls --color=auto -FXAsh"
alias ls="ls --color=auto -FXsh"
alias secure="export PS1=$'\n''  '"
alias ffmpeg="ffmpeg -loglevel quiet -stats"
alias logthefuckout="loginctl terminate-user $USER"
alias bfloat="bspc rule -a \* -o state=floating && $@"
alias joke="cat /dev/urandom | hexdump -C | grep 'ca fe'"
alias keyyy="screenkey -p fixed -g 632x76+1268+983 -t 0.3 &!"
alias yay="yay --editmenu --nodiffmenu --topdown --answeredit=All"
alias df='df --total --output="source,fstype,size,used,avail,pcent,target"'
alias ttc="tty-clock -scC7 -D"
alias reboot="systemctl reboot"
alias jrnl="vim -c 'Goyo' `date +%Y-%m-%d`.md"

####################################################################

source ~/.config/zsh/zfunctions
source ~/.config/zsh/zprofile

####################################################################

function cd() { builtin cd "$@" && ls; }

zstyle ':completion:*' rehash true

####################################################################
#
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*'

### prompt #########################################################

export PS1="%~ + "

### timer ##########################################################

function preexec() { timer=$(date +%s); }

function precmd() {
  if [ -z $timer ]; then time=0; else time=$[$(date +%s)-$timer]; fi

  RPROMPT="${time}s"
}

####################################################################

if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent > "$XDG_RUNTIME_DIR/ssh-agent.env"
    ssh-add ~/.ssh/gitlab.chanceboudreaux 2>/dev/null
fi
if [[ ! "$SSH_AUTH_SOCK" ]]; then
    source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
    ssh-add ~/.ssh/gitlab.chanceboudreaux 2>/dev/null
fi

if [ -z $(pgrep rustyvibes) ]
then
  keeb
fi
